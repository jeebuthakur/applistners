package demo.Listners;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.FormPage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class EcommerceApp extends CapEmu
{
    @BeforeTest
    public void killAllNodes() throws IOException, InterruptedException 
    {
	//taskkill /F /IM node.exe
	Runtime.getRuntime().exec("taskkill /F /IM node.exe");
	Thread.sleep(6000);
    }
    @Test
    public void totalValidation() throws InterruptedException, IOException
    {
	Service=startServer();
	AndroidDriver<AndroidElement> driver=Capabilities("GeneralStoreApp");
	//yaha per app ka name likhna hai..jo automate karni hai.
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	Thread.sleep(9000);

	//driver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
	FormPage formPage=new  FormPage(driver);
	formPage.getspinnerCountry().click();

	Utilities u=new Utilities(driver);
	u.scrollToText("Argentina");
	//driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Argentina\"))").click();

	//driver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys("Singh");
	formPage.getnameField().sendKeys("Singh");

	//driver.findElementById("com.androidsample.generalstore:id/radioFemale").click();
	formPage.getradioFemale().click();

	//driver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();
	formPage.getbtnLetsShop().click();

	//2nd Page Start Product section:--

	driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.androidsample.generalstore:id/rvProductList\")).scrollIntoView(new UiSelector().textMatches(\"Converse All Star\").instance(0))"); 
	int count= driver.findElements(By.id("com.androidsample.generalstore:id/productName")).size();

	for (int i=0;i<count;i++)
	{
	    String text= driver.findElements(By.id("com.androidsample.generalstore:id/productName")).get(i).getText();
	    if (text.equalsIgnoreCase("Converse All Star"))
	    {
		driver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(i).click();
		//break;
	    }

	}
	driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.androidsample.generalstore:id/rvProductList\")).scrollIntoView(new UiSelector().textMatches(\"Jordan Lift Off\").instance(0))"); 
	int sec= driver.findElements(By.id("com.androidsample.generalstore:id/productName")).size();
	for (int i=0; i<sec; i++)
	{
	    String text= driver.findElements(By.id("com.androidsample.generalstore:id/productName")).get(i).getText();
	    if (text.equalsIgnoreCase("Jordan Lift Off"))
	    {
		driver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(i).click();
		break;
	    }

	}

	driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
	driver.findElementByXPath("//android.widget.CheckBox[@text='Send me e-mails on discounts related to selected products in future']").click();
	driver.findElementById("com.androidsample.generalstore:id/btnProceed").click();

	driver.pressKey(new KeyEvent(AndroidKey.BACK));
	Service.stop();
    }
}


