package demo.Listners;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;

import PageObjects.HomePage;
import PageObjects.Preferences;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class ApiDemoTest extends CapEmu
{

    @Test
    public void apiDemo() throws IOException, InterruptedException
    {
	Service=startServer();
	AndroidDriver<AndroidElement> driver=Capabilities("apiDemo");
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	Thread.sleep(9000);

	//driver.findElementByXPath("//android.widget.Button[@text='CONTINUE']").click();
	//driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
	//driver.findElementByXPath("//android.widget.TextView[@text='Preference']").click();
	HomePage h=new HomePage(driver);
	h.Preferences.click();
	//driver.findElementByXPath("//android.widget.TextView[@text='3. Preference dependencies']").click();
	Preferences p=new Preferences(driver);
	p.dependencies.click();

	driver.findElementByXPath("//android.widget.CheckBox[@index='0']").click();
	driver.findElementByXPath("//android.widget.TextView[@text='WiFi settings']").click();
	driver.findElementByXPath("//android.widget.EditText").sendKeys("Hello");
	driver.findElementByXPath("android.widget.Button[@text='OK']").click();
	Service.stop();

    }
}
