package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FormPage 
{
public FormPage(AppiumDriver driver)
{
	PageFactory.initElements(new AppiumFieldDecorator(driver),this);	
}
	//driver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
      @AndroidFindBy(id="com.androidsample.generalstore:id/spinnerCountry")
      public WebElement spinnerCountry;
      
    //driver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys("Singh");
      @AndroidFindBy(id="com.androidsample.generalstore:id/nameField")
      public WebElement nameField;
      
      //driver.findElementById("com.androidsample.generalstore:id/radioFemale").click();
      @AndroidFindBy(id="com.androidsample.generalstore:id/radioFemale")
      public WebElement radioFemale;
		
		//driver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();
      @AndroidFindBy(id="com.androidsample.generalstore:id/btnLetsShop")
      public WebElement btnLetsShop;
      
      public WebElement getspinnerCountry()
      {
    	  System.out.println("Select spinnerCountry");
    	  return spinnerCountry;
      }
      public WebElement getnameField()
      {
    	  System.out.println("check Name Field");
    	  return nameField;
      }
      public WebElement getradioFemale()
      {
    	  System.out.println("Select Female button");
    	  return radioFemale;
      }
      public WebElement getbtnLetsShop()
      {
    	  System.out.println("Click on Lets shop Button");
    	  return btnLetsShop;
      }
	}


