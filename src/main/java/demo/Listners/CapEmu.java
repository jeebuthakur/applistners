package demo.Listners;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class CapEmu {

    public static AppiumDriverLocalService Service;
    public static AndroidDriver<AndroidElement> driver;

    public AppiumDriverLocalService startServer() {
	// To Start the Appium server Automatically.
	boolean flag = checkIfServerIsRunning(4723);
	if (!flag) {
	    Service = AppiumDriverLocalService.buildDefaultService();
	    Service.start();
	}
	return Service;
    }

    // Below Program for check Our Appium Server is Running or Not.
    public static boolean checkIfServerIsRunning(int port) {
	boolean isServerRunning = false;
	ServerSocket serverSocket;
	try {
	    serverSocket = new ServerSocket(port);
	    serverSocket.close();
	} catch (IOException e) {
	    // If Control comes here,Then it means that the port is in use
	    isServerRunning = true;
	} finally {
	    serverSocket = null;
	}
	return isServerRunning;
    }
    public static void startEmulator() throws IOException, InterruptedException
    {
	//C:\Users\jeebu\eclipse-workspace\Listners\src\main\java\resources\startEmulator.bat
	Runtime.getRuntime().exec(System.getProperty("user.dir") +"\\src\\main\\java\\resources\\startEmulator.bat");//This Emulator Code Only for Start Emulator Not for Real Device.
	Thread.sleep(9000);
    }

    public static AndroidDriver<AndroidElement> Capabilities(String appName) throws IOException, InterruptedException {
	// ======Ye jo Globle Name se File Bani hai Usko read karne ke leye ye code
	// likha hai========
	// ye ye system.getProperty hai vo adha path hai yaha tak ka fir use baad aage
	// ka.

	FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\java\\globle.properties");
	Properties prop = new Properties();
	// Java Method to define Properties.Globle file jo banayi hai uska path paste
	// karna hai properties me.
	prop.load(fis);
	// prop.get(appName); ye vala orgument utha ke paste kar deya 34 no. Line me or
	// cast karva deya.

	// =====end of code===================================

	// =========For Capabalities Emulator me chalani hai app ya Phone me==========

	File appDir = new File("src");// Folder Name jaha app install hai.
	File app = new File(appDir, (String) prop.get(appName));// App ka name jo jo add karni hai.//Line no. 27 vala
							// paste keya or cast kar deya.
       
	DesiredCapabilities cap = new DesiredCapabilities();
	String device = (String) prop.get("device");// Device ko bhi globle keya,Bus name change karna hai globle property me ja kar.
	if(device.contains("Thakur001"))
	{ 
	startEmulator();//To Start emulator by code.
	}
	Thread.sleep(6000);
	cap.setCapability(MobileCapabilityType.DEVICE_NAME, device);
	cap.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
	cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 14);

	AndroidDriver<AndroidElement> driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	return driver;
    }
    //ScreenShot Method implement where our app defet is comming.
    public static void getScreenshot(String s) throws IOException
    {
	
	File srcfile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(srcfile, new File(System.getProperty("user.dir")+"\\"+s+".png"));
    }
}
